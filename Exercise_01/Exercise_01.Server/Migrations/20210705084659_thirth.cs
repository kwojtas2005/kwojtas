﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Exercise_01.Server.Migrations
{
    public partial class thirth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_KW_Employee_KW_Department_DepartmentID",
                table: "KW_Employee");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentID",
                table: "KW_Employee",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_KW_Employee_KW_Department_DepartmentID",
                table: "KW_Employee",
                column: "DepartmentID",
                principalTable: "KW_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_KW_Employee_KW_Department_DepartmentID",
                table: "KW_Employee");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentID",
                table: "KW_Employee",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_KW_Employee_KW_Department_DepartmentID",
                table: "KW_Employee",
                column: "DepartmentID",
                principalTable: "KW_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
