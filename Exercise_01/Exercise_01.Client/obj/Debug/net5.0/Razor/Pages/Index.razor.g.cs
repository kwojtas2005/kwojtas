#pragma checksum "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a45038eb6f2689832b0774499f7879956d518ddd"
// <auto-generated/>
#pragma warning disable 1591
namespace Exercise_01.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\_Imports.razor"
using DevExpress.Blazor;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __Blazor.Exercise_01.Client.Pages.Index.TypeInference.CreateDxDataGrid_0(__builder, 0, 1, 
#nullable restore
#line 10 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                            EmployeeService.GetEmployeesEditableAsync

#line default
#line hidden
#nullable disable
            , 2, 
#nullable restore
#line 11 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                   OnRowRemovingAsync

#line default
#line hidden
#nullable disable
            , 3, 
#nullable restore
#line 12 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                   OnRowUpdatingAsync

#line default
#line hidden
#nullable disable
            , 4, 
#nullable restore
#line 13 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                    OnRowInsertingAsync

#line default
#line hidden
#nullable disable
            , 5, 
#nullable restore
#line 14 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                             OnInitNewRowAsync

#line default
#line hidden
#nullable disable
            , 6, "w-100", 7, 
#nullable restore
#line 16 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                          100

#line default
#line hidden
#nullable disable
            , 8, 
#nullable restore
#line 17 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                           false

#line default
#line hidden
#nullable disable
            , 9, 
#nullable restore
#line 18 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                               nameof(Employee.ID)

#line default
#line hidden
#nullable disable
            , 10, 
#nullable restore
#line 19 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                           CurrentEditMode

#line default
#line hidden
#nullable disable
            , 11, (__builder2) => {
                __builder2.OpenComponent<DevExpress.Blazor.DxDataGridCommandColumn>(12);
                __builder2.AddAttribute(13, "Width", "120px");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(14, "\r\n        ");
                __builder2.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(15);
                __builder2.AddAttribute(16, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 21 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                  nameof(Employee.ID)

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(17, "EditorVisible", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean?>(
#nullable restore
#line 21 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                                                      false

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(18, "\r\n        ");
                __Blazor.Exercise_01.Client.Pages.Index.TypeInference.CreateDxDataGridComboBoxColumn_1(__builder2, 19, 20, "Department", 21, 
#nullable restore
#line 22 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                                               nameof(Exercise_01.Shared.Employee.DepartmentID)

#line default
#line hidden
#nullable disable
                , 22, 
#nullable restore
#line 22 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                                                                                                             DepartmentService.GetDepartmentEditableAsync

#line default
#line hidden
#nullable disable
                , 23, 
#nullable restore
#line 22 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                                                                                                                                                                            nameof(Exercise_01.Shared.Department.ID)

#line default
#line hidden
#nullable disable
                , 24, 
#nullable restore
#line 22 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                                                                                                                                                                                                                                      nameof(Exercise_01.Shared.Department.Name)

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(25, "\r\n        ");
                __builder2.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(26);
                __builder2.AddAttribute(27, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 23 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                  nameof(Employee.FirstName)

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(28, "\r\n        ");
                __builder2.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(29);
                __builder2.AddAttribute(30, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 24 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                                  nameof(Employee.Surname)

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
            }
            , 31, (__value) => {
#nullable restore
#line 9 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
                       grid = __value;

#line default
#line hidden
#nullable disable
            }
            );
        }
        #pragma warning restore 1998
#nullable restore
#line 28 "C:\projects\Ex1\Exercise_01\Exercise_01.Client\Pages\Index.razor"
       
    DxDataGrid<Employee> grid;
    IEnumerable<DataGridEditMode> EditModes { get; } = Enum.GetValues(typeof(DataGridEditMode)).Cast<DataGridEditMode>();
    DataGridEditMode currentEditMode = DataGridEditMode.EditForm;
    DataGridEditMode CurrentEditMode
    {
        get => currentEditMode;
        set
        {
            if (currentEditMode != value)
            {
                currentEditMode = value;
                CancelEdit();
            }
        }
    }
    async Task OnRowRemovingAsync(Employee dataItem)
    {
        await EmployeeService.RemoveEmployeeAsync(dataItem);
        StateHasChanged();
    }
    async Task OnRowUpdatingAsync(Employee dataItem, IDictionary<string, object> newValues)
    {
        await EmployeeService.UpdateEmployeeAsync(dataItem, newValues);
        StateHasChanged();
    }
    async Task OnRowInsertingAsync(IDictionary<string, object> newValues)
    {
        await EmployeeService.InsertEmployeeAsync(newValues);
        StateHasChanged();
    }
    async Task OnInitNewRowAsync(Dictionary<string, object> values)
    {
        values.Add(nameof(Employee.FirstName), "John");
        values.Add(nameof(Employee.Surname), "Doe");
        await Task.CompletedTask;
    }
    void CancelEdit()
    {
        var cancelTask = Task.Run(async () => await grid.CancelRowEdit());
        cancelTask.GetAwaiter().GetResult();
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Services.DepartmentService DepartmentService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Services.EmployeeService EmployeeService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
namespace __Blazor.Exercise_01.Client.Pages.Index
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateDxDataGrid_0<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Func<global::System.Threading.CancellationToken, global::System.Threading.Tasks.Task<global::System.Collections.Generic.IEnumerable<T>>> __arg0, int __seq1, global::System.Func<T, global::System.Threading.Tasks.Task> __arg1, int __seq2, global::System.Func<T, global::System.Collections.Generic.IDictionary<global::System.String, global::System.Object>, global::System.Threading.Tasks.Task> __arg2, int __seq3, global::System.Func<global::System.Collections.Generic.IDictionary<global::System.String, global::System.Object>, global::System.Threading.Tasks.Task> __arg3, int __seq4, global::System.Func<global::System.Collections.Generic.Dictionary<global::System.String, global::System.Object>, global::System.Threading.Tasks.Task> __arg4, int __seq5, global::System.String __arg5, int __seq6, global::System.Int32 __arg6, int __seq7, global::System.Boolean __arg7, int __seq8, global::System.String __arg8, int __seq9, global::DevExpress.Blazor.DataGridEditMode __arg9, int __seq10, global::Microsoft.AspNetCore.Components.RenderFragment __arg10, int __seq11, global::System.Action<global::DevExpress.Blazor.DxDataGrid<T>> __arg11)
        {
        __builder.OpenComponent<global::DevExpress.Blazor.DxDataGrid<T>>(seq);
        __builder.AddAttribute(__seq0, "DataAsync", __arg0);
        __builder.AddAttribute(__seq1, "RowRemovingAsync", __arg1);
        __builder.AddAttribute(__seq2, "RowUpdatingAsync", __arg2);
        __builder.AddAttribute(__seq3, "RowInsertingAsync", __arg3);
        __builder.AddAttribute(__seq4, "InitNewRow", __arg4);
        __builder.AddAttribute(__seq5, "CssClass", __arg5);
        __builder.AddAttribute(__seq6, "PageSize", __arg6);
        __builder.AddAttribute(__seq7, "ShowPager", __arg7);
        __builder.AddAttribute(__seq8, "KeyFieldName", __arg8);
        __builder.AddAttribute(__seq9, "EditMode", __arg9);
        __builder.AddAttribute(__seq10, "ChildContent", __arg10);
        __builder.AddComponentReferenceCapture(__seq11, (__value) => { __arg11((global::DevExpress.Blazor.DxDataGrid<T>)__value); });
        __builder.CloseComponent();
        }
        public static void CreateDxDataGridComboBoxColumn_1<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Func<global::System.Threading.CancellationToken, global::System.Threading.Tasks.Task<global::System.Collections.Generic.IEnumerable<T>>> __arg2, int __seq3, global::System.String __arg3, int __seq4, global::System.String __arg4)
        {
        __builder.OpenComponent<global::DevExpress.Blazor.DxDataGridComboBoxColumn<T>>(seq);
        __builder.AddAttribute(__seq0, "Caption", __arg0);
        __builder.AddAttribute(__seq1, "Field", __arg1);
        __builder.AddAttribute(__seq2, "DataAsync", __arg2);
        __builder.AddAttribute(__seq3, "ValueFieldName", __arg3);
        __builder.AddAttribute(__seq4, "TextFieldName", __arg4);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
